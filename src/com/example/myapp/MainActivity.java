package com.example.myapp;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;

import java.math.BigInteger;
import java.util.*;
import android.R.bool;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

@SuppressLint("NewApi")
public class MainActivity extends ActionBarActivity {
	double a,b;
	boolean canedit=true;
	private EditText text,textout; 
	private Button b00,b1,b2,b3,b4,b5,b6,b7,b8,b9,b0,bC,bJian,bJia,bCheng,bChu,bDeng,bDian,bBack;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//System.out.println("123");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//System.out.println("456");
		text = (EditText) findViewById(R.id.editText1);
		textout = (EditText) findViewById(R.id.editText2);
		b1 = (Button) findViewById(R.id.Button01);
		b2 = (Button) findViewById(R.id.Button02);
		b3 = (Button) findViewById(R.id.Button03);
		b4 = (Button) findViewById(R.id.Button04);
		b5 = (Button) findViewById(R.id.Button05);
		b6 = (Button) findViewById(R.id.Button06);
		b7 = (Button) findViewById(R.id.Button07);
		b8 = (Button) findViewById(R.id.Button08);
		b9 = (Button) findViewById(R.id.Button09);
		b00 = (Button) findViewById(R.id.Button00);
		b0 = (Button) findViewById(R.id.Button0);
		bC = (Button) findViewById(R.id.ButtonC);
		bBack = (Button) findViewById(R.id.ButtonBack);
		bDian = (Button) findViewById(R.id.ButtonDian);
		bJian = (Button) findViewById(R.id.ButtonJian);
		bJia = (Button) findViewById(R.id.ButtonJia);
		bCheng = (Button) findViewById(R.id.ButtonCheng);
		bChu = (Button) findViewById(R.id.ButtonChu);
		bDeng = (Button) findViewById(R.id.ButtonDeng);
		
		//不弹出软键盘
		text.setInputType(InputType.TYPE_NULL);
		textout.setInputType(InputType.TYPE_NULL);
		
		
		bDeng.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				String str = text.getText().toString();
				if(str.length()==0) return;
				str="0"+str; 

				SplitExp(str);
				double ans=Cal();
				str="" + ans;
				textout.setText(str);
				canedit=false;
			}
		});
		bJia.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText(textout.getText().toString());
					canedit=true;
				}
				text.append("+");
			}
		});
		bJian.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText(textout.getText().toString());
					canedit=true;
				}
				text.append("-");
			}
		});
		bCheng.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText(textout.getText().toString());
					canedit=true;
				}
				text.append("*");
			}
		});
		bChu.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText(textout.getText().toString());
					canedit=true;
				}
				text.append("/");
			}
		});
		bDian.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append(".");
			}
		});
		bC.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				text.setText("");
			}
		});
		bBack.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
				}
				int index = text.length();
				Editable editable = text.getText();
				if(index!=0) editable.delete(index-1, index);
			}
		});
        b1.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("1");
			}
			
		});
        b2.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("2");
			}
		});
        b3.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("3");
			}
		});
        b4.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("4");
			}
		});
        b5.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("5");
			}
		});
        b6.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("6");
			}
		});
        b7.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("7");
			}
		});
        b8.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("8");
			}
		});
        b9.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("9");
			}
		});
        b0.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("0");
			}
		});
        b00.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(!canedit) {
					text.setText("");
					canedit=true;
				}
				text.append("00");
			}
		});
	}
	

	
	
	
	
	
	
	char post[]=new char[1000];
	Stack Op = new Stack(); //存储char类型
	public boolean Isnum(char c)
	{
		if((c>='0'&&c<='9')||c=='.')
			return true;
		return false;
	}
	public int OPMode(char c)
	{
		//if(true) return;
		if(c=='+')		return 1;
		if(c=='-')		return 2;
		if(c=='*')		return 3;
		if(c=='/')		return 4;
		return -1;
	}
	void SplitExp(String str)
	{
		
		char[] s=str.toCharArray();
		//System.out.println(s.length);
		int i,j=0;
		for(int k=0;k<1000;k++) post[k]=0;
		//if(true) return;
		//System.out.println("SSSSS");
		for(i=0;i<s.length;i++)
		{
			System.out.println(i);
			if(s[i]==' ')
				continue;
			post[j++]=' ';
			while(i<s.length&&Isnum(s[i]))
				post[j++]=s[i++];
			int curop;
			if(i<s.length)
				curop=OPMode(s[i]);
			else curop=-1;
			if(curop!=-1)
			{
				if(curop<=2)
					while(!Op.empty())
					{
						post[j++]=(Character) Op.pop();
					}
				else
				{
					while(!Op.empty()&&OPMode((Character)Op.lastElement())>2)
					{
						post[j++]=(Character) Op.pop();
					}
				}
				Op.push(s[i]);
			}
		}
		while(!Op.empty())
		{
			post[j++]=(Character) Op.pop();
		}
	}

	Stack Num = new Stack(); //存储double类型
	double Cal()
	{
		while(!Num.empty())
			Num.pop();
		int i=0,j;
		int len=0;
		while(post[i]!=0) {
			len++;
			i++;
		}
		i=0;
		
		while (i++<len)
		{
			if(post[i]==' ')
				continue;
			double cur=0,xx=10;
			boolean hasnum=false;
			boolean D=false;
			while (Isnum(post[i]))
			{
				hasnum=true;
				if(post[i]=='.') {
					D=true;
					i++;
					continue;
				}
				if(D) {
					cur+=(post[i++]-'0')/xx;
					xx*=10;
					continue;
				}
				cur*=10;
				cur+=post[i++]-'0';
			}
			if(hasnum)
				Num.push(cur);
			if(OPMode(post[i])!=-1)
			{
				double num1=(Double) Num.pop();
				double num2=(Double) Num.pop();
				switch(post[i])
				{
				case '+':		Num.push(num2+num1);break;
				case '-':		Num.push(num2-num1);break;
				case '*':		Num.push(num2*num1);break;
				case '/':		Num.push(num2/num1);break;
				}
			}
		}
		return (Double) Num.pop();
	}

	
	
	
	
	
	
	
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_about) {
			new AlertDialog.Builder(this).setTitle("关于计算器").setMessage("作者：ZhMZ\n版本1.0")
			.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}})
			.setNegativeButton("取消",null)
			.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
